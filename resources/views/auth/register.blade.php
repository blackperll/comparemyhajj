
<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" href="{{ asset('fonts/stylesheet.css') }}">
	<link rel="stylesheet" href="{{ asset('css/reuse.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/elastislide.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<script type="text/javascript" src="{{ URL::asset('http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/modernizr.custom.17475.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquerypp.custom.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('js/jquery.elastislide.js') }}"></script>

</head>

<body>
	<div class="page-wrapper">

		<div class="header">
			<p class="float-left logo-title">SEARCH</p>
			<a class="inner-logo" href=""><img src="logo-inner.png" alt="logo"></a>
			<ul class="top-menu">
				<li><a href="#" class="icon bag">MY PACKAGE</a></li>
				<li><a href="#" class="icon menu">MENU</a></li>
			</ul>
		</div>
		
		<div class="agency section-wrapper">
		    <div class="content-section">
			  <div class="content-section-top"><p>User Form</p>
			  </div>
				<div class="center-section-formbox">
					
					<div class="center-section-left">
						<p class="section-row">Company Name</p>
						<p class="section-row">Company Address</p>
						<p class="section-row">Company Tel Number</p>
						<p class="section-row">Your Name</p>
						<p class="section-row">Your Job Role</p>
						<p class="section-row">Contact Number</p>
						<p class="section-row">Email Address</p>
						<p class="section-row">Password</p>
						<p class="section-row">Varify Password</p>
						<p class="section-row"></p>
											
					</div>
					<div class="center-section-right">
						<form  class="checkbox-text" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
							
							<p class="section-row"><input type="text" name="company_name" value="{{ old('company_name') }}" /></p>
							<p class="section-row"><input type="text" name="company_address" value="{{ old('company_address') }}" /></p>
							<p class="section-row"><input type="text" name="company_tel_number" value="" /></p>
							<p class="section-row"><input type="text" name="your_name" value="" /></p>
							<p class="section-row"><input type="text" name="your_job_role" value="" /></p>
							<p class="section-row"><input type="text" name="contact_number" value="" /></p>
							<p class="section-row"><input type="email" name="email" value="" /></p>
							<p class="section-row"><input type="password" name="password" value="" /></p>
							<p class="section-row"><input type="password" name="confirm_password" value="" /></p>
							<p class="section-row"><input type="submit" name="submit" value="Submit" /></p>					
							
						</form>	
					</div>
			    </div>
			</div>
		</div>	






		
		
		<footer class="content-wrapper footer-background">			
			<div class="clearfix">&nbsp;</div><br>
			<hr>
			<div class="col-of-4-20p footer-logo">
				<a href="#"><img src="images/logo2.png" alt="logo" style="width: 250px;"></a>
				<p class="copyright-text">© 2015 Compare My Hajj</p>
			</div>
			<div class="col-of-4-23p">
				<ul class="second-footer-menu1">
					<li><a href="#">LEGAL</a></li>
					<li><a href="#">TERMS &amp; CONDITIONS</a></li>
					<li><a href="#">SECURITY</a></li>
					<li><a href="#">PRIVACY</a></li>
				</ul>
			</div>
			<div class="col-of-4-23p">
				<ul class="second-footer-menu2">
					<li><a href="#">CONTACT US</a></li>
					<li><a href="#">COOKIE</a></li>
				</ul>
			</div>
			<div class="col-of-4-34p">
				<h1>NEWS LETTER</h1>
				<form class="newsletter">
					<input type="text" placeholder="Your Email">
					<input type="submit" value="Subscribe us">
				</form>
				<div class="social clearfix">
				<div class="social-icon">
					<a href="#"><img src="images/facebook.png"></a>
					<a href="#"><img src="images/twitter.png"></a>
					<a href="#"><img src="images/google+.png"></a>
					<a href="#"><img src="images/youtube.png"></a>
				</div>
				</div>
			</div>
			<div class="clearfix">&nbsp;</div>
		</footer>
	</div>
	<script type="text/javascript">
		$(window).load(function(){
			$('#carousel').elastislide({easing : 'ease-in-out',speed : 500});
		});		
		$('a.hajj').on('click', function(){
			$('.form-search-umrah').hide();
			$('.form-search-hajj').show();
		});
		$('a.umrah').on('click', function(){
			$('.form-search-hajj').hide();
			$('.form-search-umrah').show();
		});
		$('.form-search-umrah #search-btn-umrah').on('click', function(e){ 
			$('#search-result-umrah').slideDown('slow'); 
			if($('#search-btn-umrah').hasClass("click")) {
				$('form.form-search').submit();
			}
			$('#search-btn-umrah').addClass('click');
			$('#search-btn-umrah.click').val('Lets \n Compare');			
		});
		$('.form-search-hajj #search-btn-hajj').on('click', function(e){ 
			$('#search-result-hajj').slideDown('slow'); 
			if($('#search-btn-hajj').hasClass("click")) {
				$('form.form-search').submit();
			}
			$('#search-btn-hajj').addClass('click');
			$('#search-btn-hajj.click').val('Lets \n Compare');			
		});
		$('.circle').on('hover', function(){
			$('.bg01').toggleClass('hover');
		});
		$('.circle1').on('hover', function(){
			$('.bg02').toggleClass('hover');
		});
		$('.circle2').on('hover', function(){
			$('.bg03').toggleClass('hover');
		});
		$('.circle3').on('hover', function(){
			$('.bg04').toggleClass('hover');
		});
		$('.circle').on('click', function(){ 
			$('.opacity').removeClass('hover1');
			$('.bg01').addClass('hover1');
			$('.deal-form').hide();
			$('.form1').show(); 
		});
		$('.circle1').on('click', function(){ 
			$('.opacity').removeClass('hover1');
			$('.bg02').addClass('hover1');
			$('.deal-form').hide();
			$('.form2').show(); 
		});
		$('.circle2').on('click', function(){ 
			$('.opacity').removeClass('hover1');
			$('.bg03').addClass('hover1');
			$('.deal-form').hide();
			$('.form3').show(); 
		});
		$('.circle3').on('click', function(){ 
			$('.deal-form').hide();
			$('.form4').show();
			$('.opacity').removeClass('hover1');
			$('.bg04').addClass('hover1');
		});
		$('.circle4').on('click', function(){ 
			$('.deal-form').hide();
			$('.form5').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg05').addClass('hover1');
		});
		$('.circle5').on('click', function(){ 
			$('.deal-form').hide();
			$('.form6').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg06').addClass('hover1');
		});
		$('.circle6').on('click', function(){ 
			$('.deal-form').hide();
			$('.form7').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg07').addClass('hover1');
		});
		$('.circle7').on('click', function(){ 
			$('.deal-form').hide();
			$('.form8').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg08').addClass('hover1');
		});
		$('.button-trans').on('click', function(){
			$('.button-trans').removeClass('black');
			$(this).addClass('black');
		});
		
	</script>

</body>
</html>
