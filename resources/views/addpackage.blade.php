
<!DOCTYPE html>
<html lang="" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title></title>
	<link rel="stylesheet" href="{!! asset('fonts/stylesheet.css') !!}">
	<link rel="stylesheet" href="{{ asset('css/reuse.css') }}">
	<link rel="stylesheet" href="{{ asset('css/media.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/elastislide.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.range.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/datetimepicker.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/jquery.rateyo.css') }}" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="{{ asset('js/modernizr.custom.17475.js')}}"></script>
	<script src="{{ asset('js/jquerypp.custom.js')}}"></script>
	<script src="{{ asset('js/jquery.elastislide.js')}}"></script>
	<script src="{{ asset('js/datetimepicker.js')}}"></script>
	<script src="{{ asset('js/jquery.rateyo.js')}}"></script>

</head>

<body>
<div class="page-wrapper">

	<div class="header">
		<p class="float-left logo-title">SEARCH</p>
		<a class="inner-logo" href="index.html"><img src="images/logo-inner.png" alt="logo"></a>
		<ul class="top-menu">
			<li><a href="#" class="icon bag">MY PACKAGE</a></li>
			<li><a href="#" class="icon menu">MENU</a></li>
		</ul>
	</div>



	<div class="agency section-wrapper">
		<div class="content-section">
			<div class="content-section-top"><p>Agency Form</p>
			</div>
			<div class="center-section-formbox">
				<div class="center-section-title">
					<p>Fill in the details for packages you are providing</p>
				</div>
				<div class="center-section-left">
					<p class="section-row">Package Title*</p>
					<p class="section-row">Package Description*</p>
					<p class="section-row">Package Image*</p>
					<p class="section-row">Package Star</p>
					<p class="section-row">Transport*</p>
					<p class="section-row">Airline*</p>
					<p class="section-row">Total Duration*</p>
					<p class="section-row">Duration in Makkah</p>
					<p class="section-row">Hotel in Makka Name</p>
					<p class="section-row">Number of Stars for hotel in Makkah</p>
					<p class="section-row">Distance to Masjid Makkah Hotel</p>
					<p class="section-row">Duration in Madina</p>
					<p class="section-row">Hotel in Medina Name</p>
					<p class="section-row">Number of Stars for hotel in Medina</p>
					<p class="section-row">Distance to Masjid Medina Hotel</p>
					<p class="section-row">Direct or Indirect Flights</p>
					<p class="section-row">Price Per Person (4 person room)</p>
					<p class="section-row">Price Per Person (3 person room)</p>
					<p class="section-row">Price Per Person (2 person room)</p>
					<p class="section-row">Package Start Date</p>
					<p class="section-row">Package End Date</p>
					<p class="section-row">Ziyarat in Makkah</p>
					<p class="section-row">Ziyarat in Medina</p>
					<p class="section-row">

					</p>
					<!--<p class="section-row">Visa Included</p>-->
				</div>
				<div class="center-section-right">
					<form  class="checkbox-text" method="POST" action="{{ url('/savepackage') }}" enctype="multipart/form-data">
						<p class="section-row">
							<input type="text"name="package_title" value="{{ old('package_title') }}" />
						</p>
						<p class="section-row">
							<input type="text"name="package_description" value="{{ old('package_description') }}" />
						</p>
						<p class="section-row">
							<input type="file" name="package_image"  />
						</p>

						<p class="section-row" id="package-rating">
							<input type="hidden" class="package-rating" name="package_star_rating" value="" />
						</p>

						<p class="section-row" >
							<select name="package_transport" >
								@foreach($transports as $transport)

								<option value="{{$transport->id}}">{{$transport->transport}}</option>
								@endforeach

							</select>
						</p>
						<p class="section-row" >
							<select name="package_airline">
								@foreach($airlines as $airline)

									<option value="{{$airline->id}}">{{$airline->airline}}</option>
								@endforeach
							</select>
						</p>
						<p class="section-row" >
							<input type="text" name="package_duration_total" value="{{old('package_duration_total')}}"/>
						</p>
						<p class="section-row">
							<input type="text" name="package_duration_in_makka" value="{{old('package_duration_in_makka')}}"/>
						</p>
						<p class="section-row">
							<input type="text" name="package_hotel_in_makka" value="{{old('package_hotel_in_makka')}}" />
						</p>
						<p class="section-row" id="hotel_makka_star_rating">
							<input type="hidden" class="hotel_makka_star_rating" name="package_hotel_in_makka_star_rating" value="" />
						</p>

						<p class="section-row">
							<input type="text" name="package_hotel_in_makka_distance" value="{{old('package_hotel_in_makka_distance')}}" />
						</p>
						<p class="section-row">
							<input type="text" name="package_duration_in_madina" value="{{old('	package_duration_in_madina')}}"/>
						</p>
						<p class="section-row">
							<input type="text" name="package_hotel_in_madina" value="{{old('package_hotel_in_madina')}}" />
						</p>
						<p class="section-row" id="hotel_madina_star_rating">
							<input type="hidden" class="hotel_madina_star_rating" name="package_hotel_in_madina_star_rating" value="" />
						</p>
						<p class="section-row">
							<input type="text" name="package_hotel_in_madina_distance" value="{{old('package_hotel_in_madina_distance')}}" />
						</p>
						<p class="section-row">
							<input type="text" name="package_flight_mode" value="{{old('package_flight_mode')}}" />
						</p>
						<p class="section-row">
							<input type="text" name="package_price_per_person_four_person_room" value="" />
						</p>
						<p class="section-row">
							<input type="text" name="package_price_per_person_three_person_room" value="" />
						</p>
						<p class="section-row">
							<input type="text" name="package_price_per_person_two_person_room" value="" />
						</p>
						<p class="section-row"><input type="date" name="package_start_date" class="datetimepicker" max=""></p>
						<p class="section-row"><input type="date" name="package_end_date" class="datetimepicker"  max=""></p>
						<p class="section-row">
							<input type="radio" name="package_ziyarat_in_makka" value="1">Yes
							<input type="radio" name="package_ziyarat_in_makka" value="0">No
						</p>
						<p class="section-row">
							<input type="radio" name="package_ziyarat_in_madina" value="1">Yes
							<input type="radio" name="package_ziyarat_in_madina" value="0">No
						</p>

						<p class="section-row"><input  type="submit" name="submit" value="Continue.." /></p>
						<!--<p class="section-row">
							<input type="radio" name="visa-inclusion" value="yes">Yes
							<input type="radio" name="visa-inclusion" value="no">No
						</p>-->

					</form>
				</div>
			</div>
		</div>
	</div>








	<footer class="content-wrapper footer-background">
		<div class="clearfix">&nbsp;</div><br>
		<hr>
		<div class="col-of-4-20p footer-logo">
			<a href="#"><img src="images/logo2.png" alt="logo" style="width: 250px;"></a>
			<p class="copyright-text">© 2015 Compare My Hajj</p>
		</div>
		<div class="col-of-4-23p">
			<ul class="second-footer-menu1">
				<li><a href="#">LEGAL</a></li>
				<li><a href="#">TERMS &amp; CONDITIONS</a></li>
				<li><a href="#">SECURITY</a></li>
				<li><a href="#">PRIVACY</a></li>
			</ul>
		</div>
		<div class="col-of-4-23p">
			<ul class="second-footer-menu2">
				<li><a href="#">CONTACT US</a></li>
				<li><a href="#">COOKIE</a></li>
			</ul>
		</div>
		<div class="col-of-4-34p">
			<h1>NEWS LETTER</h1>
			<form class="newsletter">
				<input type="text" placeholder="Your Email">
				<input type="submit" value="Subscribe us">
			</form>
			<div class="social clearfix">
				<div class="social-icon">
					<a href="#"><img src="images/facebook.png"></a>
					<a href="#"><img src="images/twitter.png"></a>
					<a href="#"><img src="images/google+.png"></a>
					<a href="#"><img src="images/youtube.png"></a>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
	</footer>
</div>
<script type="text/javascript">
	$(window).load(function(){
		$('#carousel').elastislide({easing : 'ease-in-out',speed : 500});
	});
	$('a.hajj').on('click', function(){
		$('.form-search-umrah').hide();
		$('.form-search-hajj').show();
	});
	$('a.umrah').on('click', function(){
		$('.form-search-hajj').hide();
		$('.form-search-umrah').show();
	});
	$('.form-search-umrah #search-btn-umrah').on('click', function(e){
		$('#search-result-umrah').slideDown('slow');
		if($('#search-btn-umrah').hasClass("click")) {
			$('form.form-search').submit();
		}
		$('#search-btn-umrah').addClass('click');
		$('#search-btn-umrah.click').val('Lets \n Compare');
	});
	$('.form-search-hajj #search-btn-hajj').on('click', function(e){
		$('#search-result-hajj').slideDown('slow');
		if($('#search-btn-hajj').hasClass("click")) {
			$('form.form-search').submit();
		}
		$('#search-btn-hajj').addClass('click');
		$('#search-btn-hajj.click').val('Lets \n Compare');
	});
	$('.circle').on('hover', function(){
		$('.bg01').toggleClass('hover');
	});
	$('.circle1').on('hover', function(){
		$('.bg02').toggleClass('hover');
	});
	$('.circle2').on('hover', function(){
		$('.bg03').toggleClass('hover');
	});
	$('.circle3').on('hover', function(){
		$('.bg04').toggleClass('hover');
	});
	$('.circle').on('click', function(){
		$('.opacity').removeClass('hover1');
		$('.bg01').addClass('hover1');
		$('.deal-form').hide();
		$('.form1').show();
	});
	$('.circle1').on('click', function(){
		$('.opacity').removeClass('hover1');
		$('.bg02').addClass('hover1');
		$('.deal-form').hide();
		$('.form2').show();
	});
	$('.circle2').on('click', function(){
		$('.opacity').removeClass('hover1');
		$('.bg03').addClass('hover1');
		$('.deal-form').hide();
		$('.form3').show();
	});
	$('.circle3').on('click', function(){
		$('.deal-form').hide();
		$('.form4').show();
		$('.opacity').removeClass('hover1');
		$('.bg04').addClass('hover1');
	});
	$('.circle4').on('click', function(){
		$('.deal-form').hide();
		$('.form5').show();
		$('.opacity').removeClass('hover1');
		$('.bg05').addClass('hover1');
	});
	$('.circle5').on('click', function(){
		$('.deal-form').hide();
		$('.form6').show();
		$('.opacity').removeClass('hover1');
		$('.bg06').addClass('hover1');
	});
	$('.circle6').on('click', function(){
		$('.deal-form').hide();
		$('.form7').show();
		$('.opacity').removeClass('hover1');
		$('.bg07').addClass('hover1');
	});
	$('.circle7').on('click', function(){
		$('.deal-form').hide();
		$('.form8').show();
		$('.opacity').removeClass('hover1');
		$('.bg08').addClass('hover1');
	});
	$('.button-trans').on('click', function(){
		$('.button-trans').removeClass('black');
		$(this).addClass('black');
	});

	/*-----------datepicker------------------*/
	jQuery('.datetimepicker').datetimepicker({

	});

	/*-----------package star rating-------------------*/
	$(function () {
		$("#package-rating").rateYo({

			halfStar: true
		});
		$("#package-rating").rateYo()
				.on("rateyo.change", function (e, data) {

					var rating = data.rating;

					$('.package-rating').val(rating);
				});

	});

	/*-----------hotel star Makkah-------------------*/
	$(function () {
		$("#hotel_makka_star_rating").rateYo({

			halfStar: true
		});
		$("#hotel_makka_star_rating").rateYo()
				.on("rateyo.change", function (e, data) {

					var rating = data.rating;

					$('.hotel_makka_star_rating').val(rating);
				});

	});

	/*-----------hotel star Maddina-------------------*/
	$(function () {
		$("#hotel_madina_star_rating").rateYo({

			halfStar: true
		});
		$("#hotel_madina_star_rating").rateYo()
				.on("rateyo.change", function (e, data) {

					var rating = data.rating;

					$('.hotel_madina_star_rating').val(rating);
				});

	});

</script>

</body>
</html>