<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body>

@yield('welcome')
@yield('innerpage')
@yield('index')
@yield('home')
@yield('changepassword')

</body>
</html>
