<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Comparemyhajj</title>
	<link rel="stylesheet" href="{!! asset('fonts/stylesheet.css') !!}">
	<link rel="stylesheet" href="{{ asset('css/reuse.css') }}">
	<link rel="stylesheet" href="{{ asset('css/media.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/elastislide.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.range.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/datetimepicker.css') }}" />

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script src="{{ asset('js/modernizr.custom.17475.js')}}"></script>
	<script src="{{ asset('js/jquerypp.custom.js')}}"></script>
	<script src="{{ asset('js/jquery.elastislide.js')}}"></script>
<script src="{{ asset('js/jquery.main.js')}}"></script>
<script src="{{ asset('js/jquery.range-min.js')}}"></script>
	<script src="{{ asset('js/datetimepicker.js')}}"></script>

	<script type="text/javascript">

$(document).ready(function(){

	$('.range-slider').jRange({
	    from: 200,
	    to: 7200,
	    step: 1,
	    format: '£%s',
	    width: "100%",
	    showLabels: true,
	    isRange : true
	});
});


	</script>	
</head>
