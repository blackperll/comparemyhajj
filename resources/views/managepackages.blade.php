@extends('layouts.innermain')
@section('managepackages')



	<div class="agency section-wrapper">
		<div class="content-section" style="min-height: 300px;">
			<div class="content-wrapper" >
			<section class="content">
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Your Packages</h3>

							<div class="box-tools">
								<div style="width: 150px;" class="input-group input-group-sm">
									<input type="text" placeholder="Search" class="form-control pull-right" name="table_search">

									<div class="input-group-btn">
										<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<tbody>
								<tr>
									<th>Image</th>
									<th>ID</th>
									<th>Package Title</th>
									<th>Date</th>
									<th>Transport</th>
									<th>Reason</th>
								</tr>
								@foreach($data as $package)


								<tr>
									<!--<td style="text-align: left;"> //{/HTML::image('$package["thumbnail"][0]->image_url.$package["thumbnail"][0]->image_type')}}</td>-->
									<td style="text-align: left;">{{ $package['package']->idpackage }}</td>
									<td style="text-align: left;">{{ $package['package']->package_title }}</td>
									<td style="text-align: left;">{{ $package['package']->LAST_UPDATE_TIMESTAMP }}</td>
									<td style="text-align: left;">{{ $package['package']->package_transport }}</td>
									<td style="text-align: left;">{{ $package['package']->package_airline }}</td>
								</tr>
                                @endforeach
								</tbody></table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</div>
</section>

		</div>
	</div>

@endsection