@extends('layouts.main')
@section('welcome')

    <div class="page-wrapper" id="foo">
        <div class="logo-fixed">
            <a class="logo" href="#"><img src="images/logo.png" alt="logo" style="width: 130px;"></a>
            <ul class="top-menu">
                <li><a href="javascript:void(0);" class="icon bag">MY PACKAGE</a></li>
                <li><a href="#" class="icon menu">MENU</a></li>
            </ul>
        </div>
    <div class="content-wrapper big-background-top section-full-screen section-01">
        <div class="clearfix">&nbsp;</div>

        <div class="section-mid">
            <p class="text-big">Welcome to our new home</p>
            <p class="text-big-bold">Compare your packages today</p>
        </div>
        <input type="button" class="search-btn desktop-hidden" id="mobile-search-btn" value="SEARCH">

        <div class="section-bottom mobile-hidden" style="position:relative">
            <a href="javascript:void(0);" class="hajj button-trans black">HAJJ</a>
            <a href="javascript:void(0);" class="umrah button-trans">UMRAH</a>
            <div class="clearfix">&nbsp;</div>
            <form class="form-search-hajj form-search">
                <div class="search-form clearfix search-details">
                    <div class="col-of-4-30p centered-text">
                        <label>Depart From</label>
                        <select name="location">
                            <option>Select Location</option>
                        </select>
                    </div>
                    <div class="col-of-4-30p centered-text">
                        <label>By Rating</label>
                        <select>
                            <option>1 Star</option>
                            <option>2 Star</option>
                            <option>3 Star</option>
                            <option>4 Star</option>
                            <option selected="selected">5 Star</option>
                        </select>
                    </div>
                    <div class="col-of-4-20p centered-text">
                        <label>Adult</label>
                        <select>
                            <option>1</option>
                            <option>2</option>
                            <option selected="selected">3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                    <div class="col-of-4-20p centered-text">
                        <label>Child</label>
                        <select>
                            <option>1</option>
                            <option selected="selected">2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                </div>
                <div class="search-form clearfix search-result" id="search-result-hajj" style="display: none;">
                    <div class="col-of-3-33p centered-text">
                        <label>Name</label>
                        <input type="text" value="" name="">
                    </div>
                    <div class="col-of-3-33p centered-text">
                        <label style="text-align: center;">Phone</label>
                        <input type="text" value="" name="" >
                    </div>
                    <div class="col-of-3-33p centered-text">
                        <label style="text-align: center;">Email Id</label>
                        <input type="text" value="" name="" >
                    </div>
                </div>
                <input type="button" class="search-btn" id="search-btn-hajj" value="SEARCH">
            </form>

            <form class="form-search-umrah form-search" style="display:none;" method="post" action="innerpage.html">
                <div class="search-form clearfix search-details">
                    <div class="col-of-4-30p centered-text">
                        <label>Depart From</label>
                        <select>
                            <option>Select Location</option>
                        </select>
                    </div>
                    <div class="col-of-4-30p centered-text">
                        <label>Depart Date</label>
                        <select>
                            <option>Select Date</option>
                        </select>
                    </div>
                    <div class="col-of-4-13p centered-text">
                        <label>Nights</label>
                        <select>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                        </select>
                    </div>
                    <div class="col-of-4-13p centered-text">
                        <label>Adult</label>
                        <select>
                            <option>1</option>
                            <option>2</option>
                            <option selected="selected">3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                    <div class="col-of-4-13p centered-text">
                        <label>Child</label>
                        <select>
                            <option>1</option>
                            <option selected="selected">2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                </div>
                <div class="search-form clearfix search-result" id="search-result-umrah" style="display: none;">
                    <div class="col-of-3-33p centered-text">
                        <label>Name</label>
                        <input type="text" value="" name="" >
                    </div>
                    <div class="col-of-3-33p centered-text">
                        <label>Phone</label>
                        <input type="text" value="" name="">
                    </div>
                    <div class="col-of-3-33p centered-text">
                        <label>Email Id</label>
                        <input type="text" value="" name="">
                    </div>
                </div>
                <input type="button" class="search-btn" id="search-btn-umrah" value="SEARCH">

            </form>
        </div>
    </div>

    <div class="content-wrapper my_packages section-full-screen section-02">
        <h2 class="centered-text heading-bold light-black">MY PACKAGE</h2>
        <p class="centered-text p-padded">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to  make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>


        <div class="package-wrapper">
            <ul id="carousel" class="elastislide-list">
                <li class="opacity bg01">
                    <img src="images/WEB-02.jpg" alt="image01" />
                    <span class="circle circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form1">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg02">
                    <img src="images/WEB-03.jpg" alt="image02" />
                    <span class="circle1 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form2">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg03">
                    <img src="images/WEB-04.jpg" alt="image03" />
                    <span class="circle2 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form3">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg04">
                    <img src="images/WEB-05.jpg" alt="image03" />
                    <span class="circle3 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form4">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg05">
                    <img src="images/WEB-02.jpg" alt="image01" />
                    <span class="circle4 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form5">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg06">
                    <img src="images/WEB-03.jpg" alt="image02" />
                    <span class="circle5 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form6">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg07">
                    <img src="images/WEB-04.jpg" alt="image03" />
                    <span class="circle6 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form7">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
                <li class="opacity bg08">
                    <img src="images/WEB-05.jpg" alt="image03" />
                    <span class="circle7 circ"><a href="javascript:void(0);" style="color:#ffffff;">5 Star<br/>Packages</a></span>
                    <div class="deal-form form8">
                        <form class="get-deal">
                            <input type="text" placeholder="Name">
                            <input type="email" placeholder="Email">
                            <input type="text" placeholder="Phone number">
                            <input type="submit" value="Get Deal">
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
        <footer class="content-wrapper footer-background section-full-screen section-03">
            <div class="clearfix">&nbsp;</div>
            <div class="col-of-4">
                <h3>CONTACT US</h3>
                <address class="footer-address">1234 South Lipsum Avenue, Meh synth Schlitz South Lipsum Forest,<br> United States , 123456</address>
                <br>
                <address class="footer-address">Phone : +123 456 7890</address>
                <address class="footer-address">Fax: +098 765 4321</address>
                <address class="footer-address">Email: info@university2.com</address>
                <address class="footer-address">Webiste: www.website.com</address>
            </div>

            <div class="col-of-4">
                <h3>ABOUT US</h3>
                <ul>
                    <li><a href="#">HISTORY &amp; MISSION</a></li>
                    <li><a href="#">EMPLOYMENT</a></li>
                    <li><a href="#">SOCIAL MEDIA</a></li>
                    <li><a href="#">COMMUNITY</a></li>
                    <li><a href="#">MANAGEMENT</a></li>
                    <li><a href="#">EMPLOYMENT</a></li>
                </ul>
            </div>
            <div class="col-of-4">
                <h3>QUICK LINK</h3>
                <ul>
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">BLOG</a></li>
                    <li><a href="#">EVENTS</a></li>
                    <li><a href="#">COURSE</a></li>
                    <li><a href="#">CAREER</a></li>
                </ul>
            </div>
            <div class="col-of-4">
                <h3>FLIGHT SCHEDULE</h3>
                <ul>
                    <li><a href="#">CITY AIRLINE ROUTES</a></li>
                    <li><a href="#">INTERNATIONAL FLIGHTS</a></li>
                    <li><a href="#">INTERNATIONAL HOTELS</a></li>
                    <li><a href="#">BUS BOOKINGS</a></li>
                    <li><a href="#">DOMESTIC AIRLINES</a></li>
                    <li><a href="#">HOTELS IN INDIA</a></li>
                    <li><a href="#">HOTEL BOOKING</a></li>
                </ul>
            </div>



            <div class="clearfix">&nbsp;</div><br>
            <hr></hr>
            <div class="col-of-4-20p footer-logo">
                <a href="#"><img src="images/logo2.png" alt="logo" style="width: 250px;"></a>
                <p class="copyright-text">&copy; 2015 Compare My Hajj</p>
            </div>
            <div class="col-of-4-23p">
                <ul class="second-footer-menu1">
                    <li><a href="#">LEGAL</a></li>
                    <li><a href="#">TERMS &amp; CONDITIONS</a></li>
                    <li><a href="#">SECURITY</a></li>
                    <li><a href="#">PRIVACY</a></li>
                </ul>
            </div>
            <div class="col-of-4-23p">
                <ul class="second-footer-menu2">
                    <li><a href="#">CONTACT US</a></li>
                    <li><a href="#">COOKIE</a></li>
                </ul>
            </div>
            <div class="col-of-4-34p">
                <h1>NEWS LETTER</h1>
                <form class="newsletter">
                    <input type="text" placeholder="Your Email">
                    <input type="submit" value="Subscribe us">
                </form>
                <div class="social clearfix">
                    <div class="social-icon">
                        <a href="#"><img src="images/facebook.png"></a>
                        <a href="#"><img src="images/twitter.png"></a>
                        <a href="#"><img src="images/google+.png"></a>
                        <a href="#"><img src="images/youtube.png"></a>
                    </div>
                </div>
            </div>
            <div class="clearfix">&nbsp;</div>
        </footer>
    </div>
    <div class="scroll-nav">
        <ul>
            <li class="screen-01 active">&nbsp;<img src="images/star-black.png"></li>
            <li class="screen-02">&nbsp;<img src="images/star-black.png"></li>
            <li class="screen-03">&nbsp;<img src="images/star-black.png"></li>
        </ul>
    </div>

    <div class="section-bottom desktop-hidden">
        <div class="mobile-search" id="mobile-search">
            <a href="javascript:void(0);" class="hajj mobile-button-trans">HAJJ</a>
            <form class="mobile-search-hajj form-search" style="display:none;" method="get" action="{{ action('WelcomeController@resultpage') }}">
                <div class="mobile-search-form clearfix mobile-search-details">
                    <div class="full-width centered-text">
                        <label>Depart From</label>
                        <select>
                            <option>Select Location</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>By Rating</label>
                        <select>
                            <option>1 Star</option>
                            <option>2 Star</option>
                            <option>3 Star</option>
                            <option>4 Star</option>
                            <option selected="selected">5 Star</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>Adult</label>
                        <select>
                            <option>1</option>
                            <option>2</option>
                            <option selected="selected">3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>Child</label>
                        <select>
                            <option>1</option>
                            <option selected="selected">2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                </div>
                <div class="mobile-search-form clearfix mobile-search-result" id="mobile-search-result-hajj" style="display: none;">
                    <div class="full-width centered-text">
                        <label>Name</label>
                        <input type="text" value="" name="">
                    </div>
                    <div class="full-width centered-text">
                        <label style="text-align: center;">Phone</label>
                        <input type="text" value="" name="" >
                    </div>
                    <div class="full-width centered-text">
                        <label style="text-align: center;">Email Id</label>
                        <input type="text" value="" name="" >
                    </div>
                </div>
                <input type="button" class="mobile-search-btn search-btn" id="mobile-search-btn-hajj" value="SEARCH">
            </form>

            <a href="javascript:void(0);" class="umrah mobile-button-trans">UMRAH</a>

            <form class="mobile-form-search-umrah form-search" style="display:none;" method="get" action="{{ action('WelcomeController@resultpage') }}">
                <div class="mobile-search-form clearfix mobile-search-details">
                    <div class="full-width centered-text">
                        <label>Depart From</label>
                        <select>
                            <option>Select Location</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>Depart Date</label>
                        <select>
                            <option>Select Date</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>Nights</label>
                        <select>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>Adult</label>
                        <select>
                            <option>1</option>
                            <option>2</option>
                            <option selected="selected">3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                    <div class="full-width centered-text">
                        <label>Child</label>
                        <select>
                            <option>1</option>
                            <option selected="selected">2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                        </select>
                    </div>
                </div>
                <div class="mobile-search-form clearfix mobile-search-result" id="mobile-search-result-umrah" style="display: none;">
                    <div class="full-width centered-text">
                        <label>Name</label>
                        <input type="text" value="" name="" >
                    </div>
                    <div class="full-width centered-text">
                        <label>Phone</label>
                        <input type="text" value="" name="">
                    </div>
                    <div class="full-width centered-text">
                        <label>Email Id</label>
                        <input type="text" value="" name="">
                    </div>
                </div>
                <input type="button" class="mobile-search-btn search-btn" id="mobile-search-btn-umrah" value="SEARCH">
            </form>
        </div>
    </div>


    <!-- <div class="page-wrrapper" style="height:2000px;"> -->

    <p class="clearfix"></p>
    </div>
    <div class="page-wrapper main-menu-wrap" style="display:none;">
        <div class="content-wrapper menu-background-top main-menu">
            <a class="logo" href="#"><img src="images/logo.png" alt="logo" style="width: 130px;"></a>
            <ul class="top-menu">
                <li><a href="#" class="close menu">MENU</a></li>
            </ul>
            <div class="clearfix">&nbsp;</div>
            <br>


            <div class="section-mid-menu">
                <ul>
                    <li class="text-big-menu">
                        <hr></hr>
                        <span>Hajj</span>
                    </li>
                    <ul class="text-big-bold-menu">
                        <li><a href="#">5 Star Packages</a></li>
                        <li><a href="#">4 Star Packages</a></li>
                        <li><a href="#">3 Star Packages</a></li>
                        <li><a href="#">2 Star Packages</a></li>
                        <li><a href="#">1 Star Packages</a></li>
                        <li><a href="#">5 Star Packages</a></li>
                        <li><a href="#">4 Star Packages</a></li>
                        <li><a href="#">3 Star Packages</a></li>
                        <li><a href="#">2 Star Packages</a></li>
                        <li><a href="#">1 Star Packages</a></li>
                        <li><a href="#">5 Star Packages</a></li>
                        <li><a href="#">4 Star Packages</a></li>
                    </ul>


                    <li class="text-big-menu">
                        <span>Umrah</span>
                    </li>
                    <ul class="text-big-bold-menu">
                        <li><a href="#">Family holidays</a></li>
                        <li><a href="#">5 Star Packages</a></li>
                        <li><a href="#">4 Star Packages</a></li>
                        <li><a href="#">3 Star Packages</a></li>
                        <li><a href="#">2 Star Packages</a></li>
                        <li><a href="#">1 Star Packages</a></li>
                        <li><a href="#">5 Star Packages</a></li>
                        <li><a href="#">4 Star Packages</a></li>
                        <li><a href="#">3 Star Packages</a></li>
                        <li><a href="#">2 Star Packages</a></li>
                        <li><a href="#">1 Star Packages</a></li>
                        <li><a href="#">5 Star Packages</a></li>
                    </ul>

                    <li class="text-big-menu">	</li>

                </ul>

            </div>
        </div>
    </div>

    <div class="mysuitcase-overlay">
        <div class="page-wrapper my-suitcase-wrap">
            <div class="my-suitcase">
                <div class="suitcase-head">
                    <div class="float-left suitcase-icon"><img src="images/suitcase-blue.png"></div>
                    <div class="float-right settings-icon"><img src="images/settings-blue.png"></div>
                </div>
                <div class="suitcase-title">
                    <h3>My Packages</h3>
                    <div class="suitcase-controls">
                        <div class="float-left">
                            <img src="images/suitcase-edit-blue.png">
                            <img src="images/suitcase-user-blue.png">
                            <img src="images/suitcase-comment-blue.png">
                        </div>
                        <div class="float-right">
                            <img src="images/suitcase-email-blue.png">
                        </div>
                    </div>
                </div>
                <div class="deals-wrap">
                    <div class="deal-block">
                        <div class="deal-title">
                            <div class="deal-details">
                                <p>5 Star Deal</p>
								<span class="star-icon">
									<img src="images/star-rating.png">
									<img src="images/star-rating.png">
									<img src="images/star-rating.png">
								</span>
                            </div>
                            <img src="images/deal-img-01.jpg">
                            <span class="close-icon"><img src="images/deal-close-icon.png"></span>
                            <p class="deal-price">£1498</p>
                        </div>
                        <div class="deal-desc">
                            <div class="deal-date">
                                <img src="images/deal-date.png">
                            </div>
                            <div class="view-deal">
                                <input type="button" name="view-deal" class="view-package-btn" value="View Deal">
                            </div>
                            <!-- <div class="deal-price-quote">£1498</div>
                            <div class="view-deal">VIEW DEAL</div> -->
                        </div>
                    </div>

                    <div class="deal-block">
                        <div class="deal-title">
                            <div class="deal-details">
                                <p>5 Star Deal</p>
								<span class="star-icon">
									<img src="images/star-rating.png">
									<img src="images/star-rating.png">
									<img src="images/star-rating.png">
								</span>
                            </div>
                            <img src="images/deal-img-01.jpg">
                            <span class="close-icon"><img src="images/deal-close-icon.png"></span>
                            <p class="deal-price">£1498</p>
                        </div>
                        <div class="deal-desc">
                            <div class="deal-date">
                                <img src="images/deal-date.png">
                            </div>
                            <div class="view-deal">
                                <input type="button" name="view-deal" class="view-package-btn" value="View Deal">
                            </div>
                            <!-- <div class="deal-price-quote">£1498</div>
                            <div class="view-deal">VIEW DEAL</div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(window).load(function(){
            $('#carousel').elastislide({easing : 'ease-in-out',speed : 500});
        });
        $('a.hajj').on('click', function(){
            $('.form-search-umrah').hide();
            $('.form-search-hajj').show();
        });
        $('a.umrah').on('click', function(){
            $('.form-search-hajj').hide();
            $('.form-search-umrah').show();
        });
        $('.form-search-umrah #search-btn-umrah').on('click', function(e){
            $('.form-search-umrah .search-details').slideUp('slow');
            $('#search-result-umrah').slideDown('slow');
            if($('#search-btn-umrah').hasClass("click")) {
                $('form.form-search').submit();
            }
            $('#search-btn-umrah').addClass('click');
            $('#search-btn-umrah.click').val('Lets \n Compare');
        });
        $('.form-search-hajj #search-btn-hajj').on('click', function(e){

            $('.form-search-hajj .search-details').slideUp('slow');
            $('.form-search-hajj #search-result-hajj').slideDown('slow');
            if($('#search-btn-hajj').hasClass("click")) {
                $('form.form-search').submit();
            }
            $('#search-btn-hajj').addClass('click');
            $('#search-btn-hajj.click').val('Lets \n Compare');
        });
        $('.circle').on('hover', function(){
            $('.bg01').toggleClass('hover');
        });
        $('.circle1').on('hover', function(){
            $('.bg02').toggleClass('hover');
        });
        $('.circle2').on('hover', function(){
            $('.bg03').toggleClass('hover');
        });
        $('.circle3').on('hover', function(){
            $('.bg04').toggleClass('hover');
        });
        $('.circle').on('click', function(){
            $('.opacity').removeClass('hover1');
            $('.bg01').addClass('hover1');
            $('.deal-form').hide();
            $('.form1').show();
        });
        $('.circle1').on('click', function(){
            $('.opacity').removeClass('hover1');
            $('.bg02').addClass('hover1');
            $('.deal-form').hide();
            $('.form2').show();
        });
        $('.circle2').on('click', function(){
            $('.opacity').removeClass('hover1');
            $('.bg03').addClass('hover1');
            $('.deal-form').hide();
            $('.form3').show();
        });
        $('.circle3').on('click', function(){
            $('.deal-form').hide();
            $('.form4').show();
            $('.opacity').removeClass('hover1');
            $('.bg04').addClass('hover1');
        });
        $('.circle4').on('click', function(){
            $('.deal-form').hide();
            $('.form5').show();
            $('.opacity').removeClass('hover1');
            $('.bg05').addClass('hover1');
        });
        $('.circle5').on('click', function(){
            $('.deal-form').hide();
            $('.form6').show();
            $('.opacity').removeClass('hover1');
            $('.bg06').addClass('hover1');
        });
        $('.circle6').on('click', function(){
            $('.deal-form').hide();
            $('.form7').show();
            $('.opacity').removeClass('hover1');
            $('.bg07').addClass('hover1');
        });
        $('.circle7').on('click', function(){
            $('.deal-form').hide();
            $('.form8').show();
            $('.opacity').removeClass('hover1');
            $('.bg08').addClass('hover1');
        });
        $('.button-trans').on('click', function(){
            $('.button-trans').removeClass('black');
            $(this).addClass('black');
        });

        var wh = $(window).height();
        $('.section-full-screen').height(wh);
        var current_screen = 1;
        $('.scroll-nav li').on('click', function() {
            if($(this).attr('class')=='screen-01'){
                $('.section-01').animate({
                    bottom : '0'
                }, 800);
                $('.logo img').attr('src', 'images/logo.png');
                $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag.png)','color':'#FFFFFF'});
                $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch.png)','color':'#FFFFFF'});
                $('.screen-02, .screen-03').removeClass('active');
                $('.screen-01').addClass('active');
                $('.scroll-nav ul li').css('border-color', '#FFF');
                current_screen = 1;
            } else if($(this).attr('class')=='screen-02'){
                $('.section-01').animate({
                    bottom : '100%'
                }, 800);
                $('.section-02').animate({
                    bottom : '0'
                }, 800);
                $('.logo img').attr('src', 'images/logo1.png');
                $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag1.png)','color':'#4F4266'});
                $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch1.png)','color':'#4F4266'});
                $('.screen-01, .screen-03').removeClass('active');
                $('.screen-02').addClass('active');
                $('.scroll-nav ul li').css('border-color', '#996699');
                current_screen = 2;
            } else if($(this).attr('class')=='screen-03'){
                $('.section-01').animate({
                    bottom : '100%'
                }, 800);
                $('.section-02').animate({
                    bottom : '100%'
                }, 800);
                $('.logo img').attr('src', 'images/logo.png');
                $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag.png)','color':'#FFFFFF'});
                $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch.png)','color':'#FFFFFF'});
                $('.screen-01, .screen-02').removeClass('active');
                $('.scroll-nav ul li').css('border-color', '#FFF');
                $('.screen-03').addClass('active');
                current_screen = 3;
            }
        });
        var window_width = $(window).width();
        if(window_width>760) {
            $('#foo').bind('mousewheel', function(e){
                if(e.originalEvent.wheelDelta /120 > 0) {
                    // alert('up');
                    if (current_screen == 2) {
                        $('.section-01').animate({
                            bottom : '0'
                        }, 800);
                        $('.logo img').attr('src', 'images/logo.png');
                        $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag.png)','color':'#FFFFFF'});
                        $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch.png)','color':'#FFFFFF'});
                        $('.screen-02, .screen-03').removeClass('active');
                        $('.screen-01').addClass('active');
                        $('.scroll-nav ul li').css('border-color', '#FFF');
                        current_screen = 1;
                    } else if (current_screen == 3) {
                        $('.section-02').animate({
                            bottom : '0'
                        }, 800);
                        $('.logo img').attr('src', 'images/logo1.png');
                        $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag1.png)','color':'#4F4266'});
                        $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch1.png)','color':'#4F4266'});
                        $('.screen-01, .screen-03').removeClass('active');
                        $('.screen-02').addClass('active');
                        $('.scroll-nav ul li').css('border-color', '#996699');
                        current_screen = 2;
                    }
                }
                else{
                    // alert('down');
                    if (current_screen == 1) {
                        $('.section-01').animate({
                            bottom : '100%'
                        }, 800);
                        $('.logo img').attr('src', 'images/logo1.png');
                        $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag1.png)','color':'#4F4266'});
                        $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch1.png)','color':'#4F4266'});
                        $('.screen-01, .screen-03').removeClass('active');
                        $('.scroll-nav ul li').css('border-color', '#996699');
                        $('.screen-02').addClass('active');
                        current_screen = 2;
                    } else if (current_screen == 2) {
                        $('.section-02').animate({
                            bottom : '100%'
                        }, 800);
                        $('.logo img').attr('src', 'images/logo.png');
                        $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag.png)','color':'#FFFFFF'});
                        $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch.png)','color':'#FFFFFF'});
                        $('.screen-01, .screen-02').removeClass('active');
                        $('.screen-03').addClass('active');
                        $('.scroll-nav ul li').css('border-color', '#FFF');
                        current_screen = 3;
                    };
                }
            });
        }
        $('.page-wrapper').on('click', function(e){
            if(event.target.id!='mobile-search-btn'){
                $('#mobile-search').removeClass('visible');
            }
        });
        $('#mobile-search-btn').on('click', function() {
            $('.desktop-hidden #mobile-search').toggleClass('visible');
        });

        $('.hajj.mobile-button-trans').on('click', function(){
            $('.mobile-search-hajj').slideToggle();
            $('.umrah.mobile-button-trans').toggle();
        });
        $('.umrah.mobile-button-trans').on('click', function(){
            $('.mobile-form-search-umrah').slideToggle();
            $('.hajj.mobile-button-trans').toggle();
        });

        $(window).scroll(function() {
            if ($(this).scrollTop() > 0) {
                // alert('s');
            }
        });

        if(window_width>760) {
            $(document).keydown(function(event){
                var key = event.which;
                switch(key) {
                    case 37:
                        // alert('Key left');
                        // Key left.
                        break;
                    case 38:
                        if (current_screen == 2) {
                            $('.section-01').animate({
                                bottom : '0'
                            }, 800);
                            $('.logo img').attr('src', 'images/logo.png');
                            $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag.png)','color':'#FFFFFF'});
                            $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch.png)','color':'#FFFFFF'});
                            $('.screen-02, .screen-03').removeClass('active');
                            $('.screen-01').addClass('active');
                            $('.scroll-nav ul li').css('border-color', '#FFF');
                            current_screen = 1;
                        } else if (current_screen == 3) {
                            $('.section-02').animate({
                                bottom : '0'
                            }, 800);
                            $('.logo img').attr('src', 'images/logo1.png');
                            $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag1.png)','color':'#4F4266'});
                            $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch1.png)','color':'#4F4266'});
                            $('.screen-01, .screen-03').removeClass('active');
                            $('.screen-02').addClass('active');
                            $('.scroll-nav ul li').css('border-color', '#996699');
                            current_screen = 2;
                        }
                        // Key up.
                        break;
                    case 39:
                        // alert('Key right');
                        // Key right.
                        break;
                    case 40:
                        // alert('down');
                        if (current_screen == 1) {
                            $('.section-01').animate({
                                bottom : '100%'
                            }, 800);
                            $('.logo img').attr('src', 'images/logo1.png');
                            $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag1.png)','color':'#4F4266'});
                            $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch1.png)','color':'#4F4266'});
                            $('.screen-01, .screen-03').removeClass('active');
                            $('.screen-02').addClass('active');
                            $('.scroll-nav ul li').css('border-color', '#996699');
                            current_screen = 2;
                        } else if (current_screen == 2) {
                            $('.section-02').animate({
                                bottom : '100%'
                            }, 800);
                            $('.logo img').attr('src', 'images/logo.png');
                            $('.top-menu li a.icon.bag').css({'background-image':'url(images/menu-icons-bag.png)','color':'#FFFFFF'});
                            $('.top-menu li a.icon.menu').css({'background-image':'url(images/menu-icons-sandwitch.png)','color':'#FFFFFF'});
                            $('.screen-01, .screen-02').removeClass('active');
                            $('.screen-03').addClass('active');
                            $('.scroll-nav ul li').css('border-color', '#FFF');
                            current_screen = 3;
                        };
                        // Key down.
                        break;
                }
            });
        }

        $('.close.menu').on('click', function(){
            $('.main-menu-wrap').slideUp();
        });
        $('.top-menu li a.icon.menu').on('click', function(){
            $('.main-menu-wrap').slideDown();
        });

        $('.mysuitcase-overlay').on('click', function(){
            $(this).animate({left : '100%'},100);
        });

        $('.top-menu li a.icon.bag').on('click', function(){
            $('.mysuitcase-overlay').animate({left : '0'},100);
        });

    </script>


@endsection

