@extends('layouts.main')
@section('innerpage')

	<div class="page-wrapper search-page open">

	<div class= "header">
		<p class="float-left logo-title">SEARCH</p>
		<a class="inner-logo" href="index.html"><img src="images/logo-inner.png" alt="logo" ></a>
		<ul class="top-menu">
			<li><a href="#" class="icon bag">MY PACKAGE</a></li>
			<li><a href="#" class="icon menu">MENU</a></li>
		</ul>
	</div>
	<div class="section-wrapper">


		<div class="right-section">
			<div class="right-section-top"><p>123 properties with 882 offers</p></div>
			<div class="right-section-inner">
				<div class="right-section-title">
					<p class="right-sec-title">HAJJ</p>
					<p>23rd Dec to 30th Dec</p>
				</div>
				<div class="daily-date">
					<ul>
						<li class="daily-search">Sun 20 Dec From £234pp</li>
						<li class="daily-search">Sun 20 Dec From £234pp</li>
						<li class="daily-search">Sun 20 Dec From £234pp</li>
						<li class="daily-search">Sun 20 Dec From £234pp</li>
						<li class="daily-search active">Sun 20 Dec From £234pp</li>
						<li class="daily-search">Sun 20 Dec From £234pp</li>
						<li class="daily-search">Sun 20 Dec From £234pp</li>
					</ul>
				</div>
				<div class="right-section-box">
					<div class="right-sec-preview">
						<img src="images/hotel-2.jpg" alt="" />
						<h4 class="package-title">
							<span>hajj family packages</span>
							<span class="star-rating"><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/></span>
						</h4>

						<ul class="package-end">
							<li><a href="javascript:void(0);">Photos</a></li>
							<li><a href="javascript:void(0);">Compare Offers</a></li>
						</ul>
						<span class="select-package"><a href="javascript:void(0);"><img src="images/menu-icons-bag.png" alt="" /></a></span>

					</div>
					<div class="right-sec-details">
						<p class="deatils">£206Avg PP</p>
						<strike>Was £207pp</strike>
						<p class="price">£413 for all</p>
						<p class="price-details">Price Includes</p>
						<p>Flights from London Gatwick<br>
							-Room Only<br>
							2 Bedroom Apartment</p>
						<p class="price-info">Join ebookers Free Bonus+
							programme and earn
							2% cash rewards
						</p>
						<a class="packages" href="javascript:void(0);">view Packages</a>
					</div>
				</div>

				<div class="right-section-box">

					<div class="right-sec-preview">
						<img src="images/hotel-1.jpg" alt="" />
						<h4 class="package-title">
							<span>hajj family packages</span>
							<span class="star-rating"><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/></span>
						</h4>

						<ul class="package-end">
							<li><a href="javascript:void(0);">Photos</a></li>
							<li><a href="javascript:void(0);">Compare Offers</a></li>
						</ul>
						<span class="select-package"><a href="javascript:void(0);"><img src="images/menu-icons-bag.png" alt="" /></a></span>

					</div>
					<div class="right-sec-details">
						<p class="deatils">£206Avg PP</p>
						<strike>Was £207pp</strike>
						<p class="price">£413 for all</p>
						<p class="price-details">Price Includes</p>
						<p>Flights from London Gatwick<br>
							-Room Only<br>
							2 Bedroom Apartment</p>
						<p class="price-info">Join ebookers Free Bonus+
							programme and earn
							2% cash rewards
						</p>
						<a class="packages" href="javascript:void(0);">view Packages</a>
					</div>
				</div>

				<div class="right-section-box">
					<div class="right-sec-preview">
						<img src="images/hotel-3.jpg" alt="" />
						<h4 class="package-title">
							<span>hajj family packages</span>
							<span class="star-rating"><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/></span>
						</h4>

						<ul class="package-end">
							<li><a href="javascript:void(0);">Photos</a></li>
							<li><a href="javascript:void(0);">Compare Offers</a></li>
						</ul>
						<span class="select-package"><a href="javascript:void(0);"><img src="images/menu-icons-bag.png" alt="" /></a></span>
					</div>
					<div class="right-sec-details">
						<p class="deatils">£206Avg PP</p>
						<strike>Was £207pp</strike>
						<p class="price">£413 for all</p>
						<p class="price-details">Price Includes</p>
						<p>Flights from London Gatwick<br>
							-Room Only<br>
							2 Bedroom Apartment</p>
						<p class="price-info">Join ebookers Free Bonus+
							programme and earn
							2% cash rewards
						</p>
						<a class="packages" href="javascript:void(0);">view Packages</a>
					</div>
				</div>

				<div class="right-section-box">
					<div class="right-sec-preview">
						<img src="images/hotel-4.jpg" alt="" />
						<h4 class="package-title">
							<span>hajj family packages</span>
							<span class="star-rating"><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/><img src="images/star-rating.png" alt=""/></span>
						</h4>

						<ul class="package-end">
							<li><a href="javascript:void(0);">Photos</a></li>
							<li><a href="javascript:void(0);">Compare Offers</a></li>
						</ul>
						<span class="select-package"><a href="javascript:void(0);"><img src="images/menu-icons-bag.png" alt="" /></a></span>
					</div>
					<div class="right-sec-details">
						<p class="deatils">£206Avg PP</p>
						<strike>Was £207pp</strike>
						<p class="price">£413 for all</p>
						<p class="price-details">Price Includes</p>
						<p>Flights from London Gatwick<br>
							-Room Only<br>
							2 Bedroom Apartment</p>
						<p class="price-info">Join ebookers Free Bonus+
							programme and earn
							2% cash rewards
						</p>
						<a class="packages" href="javascript:void(0);">view Packages</a>
					</div>
				</div>

				<div class="package-pagination">
					<ul>
						<li class="arrow"><a href="javascript:void(0);">&lt;</a></li>
						<li class="active"><a href="javascript:void(0);">1</a></li>
						<li><a href="javascript:void(0);">2</a></li>
						<li><a href="javascript:void(0);">3</a></li>
						<li><a href="javascript:void(0);">4</a></li>
						<li><a href="javascript:void(0);">5</a></li>
						<li class="arrow"><a href="javascript:void(0);">&gt;</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>



	<footer class="content-wrapper footer-background">
		<div class="clearfix">&nbsp;</div><br>
		<hr></hr>
		<div class="col-of-4-20p footer-logo">
			<a href="#"><img src="images/logo2.png" alt="logo" style="width: 250px;"></a>
			<p class="copyright-text">&copy; 2015 Compare My Hajj</p>
		</div>
		<div class="col-of-4-23p">
			<ul class="second-footer-menu1">
				<li><a href="#">LEGAL</a></li>
				<li><a href="#">TERMS &amp; CONDITIONS</a></li>
				<li><a href="#">SECURITY</a></li>
				<li><a href="#">PRIVACY</a></li>
			</ul>
		</div>
		<div class="col-of-4-23p">
			<ul class="second-footer-menu2">
				<li><a href="#">CONTACT US</a></li>
				<li><a href="#">COOKIE</a></li>
			</ul>
		</div>
		<div class="col-of-4-34p">
			<h1>NEWS LETTER</h1>
			<form class="newsletter">
				<input type="text" placeholder="Your Email">
				<input type="submit" value="Subscribe us">
			</form>
			<div class="social clearfix">
				<div class="social-icon">
					<a href="#"><img src="images/facebook.png"></a>
					<a href="#"><img src="images/twitter.png"></a>
					<a href="#"><img src="images/google+.png"></a>
					<a href="#"><img src="images/youtube.png"></a>
				</div>
			</div>
		</div>
		<div class="clearfix">&nbsp;</div>
	</footer>
	</div>
	<div class="left-section slideOpen">
		<a class="right-arrow" href="#"><img src="images/arrow-right-inner.png" alt="logo" ></a>
		<br><br>
		<p class="left-text-big">REFINE SEARCH</p>
		<p class="price-text">Price</p>
		<p style="width:80%;">
			<label>£200 - £7200</label>
			<input type="hidden" class="slider-input range-slider" min="200" value="23" />
			<!--<input type="range" value="0" min="200" max="700">-->
		</p>
		<hr></hr>
		<p>
			<span style="color:#ffffff; font-size:19px;">BOARD BASIS</span>
			<span class="float-right"><a href="javascript:void(0)">Select All <i class="fa fa-caret-down"></i></a></span>
		</p>

		<form action="" class="checkbox-text">
			<div class="float-left">
				<p><input type="checkbox" name="" value=""><label>Room Only</label></p>
				<p><input type="checkbox" name="" value=""><label>Self Catering</label></p>
				<p><input type="checkbox" name="" value=""><label>Breakfast Included (B&amp;B)</label></p>
				<p><input type="checkbox" name="" value=""><label>Half Board</label></p>
				<p><input type="checkbox" name="" value=""><label>Full Board</label></p>
				<p><input type="checkbox" name="" value=""><label>All Inclusive</label></p>
			</div>
			<div class="float-right">
				<p>From £200</p>
				<p>From £250</p>
				<p>From £290</p>
				<p>From £310</p>
				<p>From £390</p>
				<p>From £400</p>
			</div>
		</form>


		<hr></hr>
		<span style="color:#ffffff; font-size:19px;">STARS</span>
		<span class="float-right"><a href="javascript:void(0)">Select All <i class="fa fa-caret-down"></i></a></span>

		<form action="" class="checkbox-text">
			<div class="float-left">
				<p><input type="checkbox" name="" value="">&#9734;</p>
				<p><input type="checkbox" name="" value="">&#9734; &#9734;</p>
				<p><input type="checkbox" name="" value="">&#9734; &#9734; &#9734;</p>
				<p><input type="checkbox" name="" value="">&#9734; &#9734; &#9734; &#9734;</p>
				<p><input type="checkbox" name="" value="">&#9734; &#9734; &#9734; &#9734; &#9734;</p>
			</div>
			<div class="float-right">
				<p>From £200</p>
				<p>From £250</p>
				<p>From £290</p>
				<p>From £310</p>
				<p>From £390</p>
			</div>
		</form>

		<hr></hr>
		<span style="color:#ffffff; font-size:19px;">FREE FACILITIES</span>
		<span class="float-right"><a href="javascript:void(0)">Select All <i class="fa fa-caret-down"></i></a></span>
		<form action="" class="checkbox-text">
			<div class="float-left">
				<p><input type="checkbox" name="" value="">Free WIFI</p>
				<p><input type="checkbox" name="" value="">Free Internet</p>
			</div>
			<div class="float-right">
				<p>From £200</p>
				<p>From £250</p>
			</div>
		</form>

		<hr></hr>
		<span style="color:#ffffff; font-size:19px;">HOTEL FACILITIES</span>
		<span class="float-right"><a href="javascript:void(0)">Select All <i class="fa fa-caret-down"></i></a></span>
		<form action="" class="checkbox-text">
			<div class="float-left">
				<p><input type="checkbox" name="" value="">Internet</p>
				<p><input type="checkbox" name="" value="">Restaurant</p>
				<p><input type="checkbox" name="" value="">Accessible</p>
				<p><input type="checkbox" name="" value="">24hr Reception</p>
				<p><input type="checkbox" name="" value="">Airport Transfer</p>
			</div>
			<div class="float-right">
				<p>From £200</p>
				<p>From £250</p>
				<p>From £290</p>
				<p>From £310</p>
				<p>From £390</p>
			</div>
		</form>

		<hr></hr>
		<span style="color:#ffffff; font-size:19px;">DEPARTURE AIRPORTS</span>
		<span class="float-right"><a href="javascript:void(0)">Select All<i class="fa fa-caret-down"></i></a></span>
		<form action="" class="checkbox-text">
			<div class="float-left">
				<p><input type="checkbox" name="" value="">London Gatwick</p>
				<p><input type="checkbox" name="" value="">London City</p>
				<p><input type="checkbox" name="" value="">London Heathrow</p>
				<p><input type="checkbox" name="" value="">London Stansted</p>
			</div>
			<div class="float-right">
				<p>From £200</p>
				<p>From £250</p>
				<p>From £290</p>
				<p>From £310</p>
			</div>
		</form>
	</div>
	<div class="side-search" id="side-search">
		<a href="javascript:void(0);" class="hajj side-button-trans">HAJJ</a>
		<form class="side-search-hajj form-search" style="display:none;" method="post" action="innerpage.html">
			<div class="side-search-form clearfix side-search-details">
				<div class="full-width centered-text">
					<label>Depart From</label>
					<select>
						<option>Select Location</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>By Rating</label>
					<select>
						<option>1 Star</option>
						<option>2 Star</option>
						<option>3 Star</option>
						<option>4 Star</option>
						<option selected="selected">5 Star</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>Adult</label>
					<select>
						<option>1</option>
						<option>2</option>
						<option selected="selected">3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>Child</label>
					<select>
						<option>1</option>
						<option selected="selected">2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
					</select>
				</div>
			</div>
			<div class="side-search-form clearfix side-search-result" id="side-search-result-hajj" style="display: none;">
				<div class="full-width centered-text">
					<label>Name</label>
					<input type="text" value="" name="">
				</div>
				<div class="full-width centered-text">
					<label style="text-align: center;">Phone</label>
					<input type="text" value="" name="" >
				</div>
				<div class="full-width centered-text">
					<label style="text-align: center;">Email Id</label>
					<input type="text" value="" name="" >
				</div>
			</div>
			<input type="button" class="side-search-btn search-btn" id="side-search-btn-hajj" value="SEARCH">
		</form>

		<a href="javascript:void(0);" class="umrah side-button-trans">UMRAH</a>

		<form class="side-form-search-umrah form-search" style="display:none;" method="post" action="innerpage.html">
			<div class="side-search-form clearfix side-search-details">
				<div class="full-width centered-text">
					<label>Depart From</label>
					<select>
						<option>Select Location</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>Depart Date</label>
					<select>
						<option>Select Date</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>Nights</label>
					<select>
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>Adult</label>
					<select>
						<option>1</option>
						<option>2</option>
						<option selected="selected">3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
					</select>
				</div>
				<div class="full-width centered-text">
					<label>Child</label>
					<select>
						<option>1</option>
						<option selected="selected">2</option>
						<option>3</option>
						<option>4</option>
						<option>5</option>
						<option>6</option>
						<option>7</option>
						<option>8</option>
					</select>
				</div>
			</div>
			<div class="side-search-form clearfix side-search-result" id="side-search-result-umrah" style="display: none;">
				<div class="full-width centered-text">
					<label>Name</label>
					<input type="text" value="" name="" >
				</div>
				<div class="full-width centered-text">
					<label>Phone</label>
					<input type="text" value="" name="">
				</div>
				<div class="full-width centered-text">
					<label>Email Id</label>
					<input type="text" value="" name="">
				</div>
			</div>
			<input type="button" class="side-search-btn search-btn" id="side-search-btn-umrah" value="SEARCH">
		</form>
	</div>
	<div class="page-wrapper main-menu-wrap" style="display:none;">
		<div class="content-wrapper menu-background-top main-menu">
			<a class="logo" href="#"><img src="images/logo.png" alt="logo" style="width: 130px;"></a>
			<ul class="top-menu">
				<li><a href="#" class="close menu">MENU</a></li>
			</ul>
			<div class="clearfix">&nbsp;</div>
			<br>


			<div class="section-mid-menu">
				<ul>
					<li class="text-big-menu">
						<hr></hr>
						<span>Hajj</span>
					</li>
					<ul class="text-big-bold-menu">
						<li><a href="#">5 Star Packages</a></li>
						<li><a href="#">4 Star Packages</a></li>
						<li><a href="#">3 Star Packages</a></li>
						<li><a href="#">2 Star Packages</a></li>
						<li><a href="#">1 Star Packages</a></li>
						<li><a href="#">5 Star Packages</a></li>
						<li><a href="#">4 Star Packages</a></li>
						<li><a href="#">3 Star Packages</a></li>
						<li><a href="#">2 Star Packages</a></li>
						<li><a href="#">1 Star Packages</a></li>
						<li><a href="#">5 Star Packages</a></li>
						<li><a href="#">4 Star Packages</a></li>
					</ul>


					<li class="text-big-menu">
						<span>Umrah</span>
					</li>
					<ul class="text-big-bold-menu">
						<li><a href="#">Family holidays</a></li>
						<li><a href="#">5 Star Packages</a></li>
						<li><a href="#">4 Star Packages</a></li>
						<li><a href="#">3 Star Packages</a></li>
						<li><a href="#">2 Star Packages</a></li>
						<li><a href="#">1 Star Packages</a></li>
						<li><a href="#">5 Star Packages</a></li>
						<li><a href="#">4 Star Packages</a></li>
						<li><a href="#">3 Star Packages</a></li>
						<li><a href="#">2 Star Packages</a></li>
						<li><a href="#">1 Star Packages</a></li>
						<li><a href="#">5 Star Packages</a></li>
					</ul>

					<li class="text-big-menu">	</li>

				</ul>

			</div>
		</div>
	</div>

	<div class="mysuitcase-overlay">
		<div class="page-wrapper my-suitcase-wrap">
			<div class="my-suitcase">
				<div class="suitcase-head">
					<div class="float-left suitcase-icon"><img src="images/suitcase-blue.png"></div>
					<div class="float-right settings-icon"><img src="images/settings-blue.png"></div>
				</div>
				<div class="suitcase-title">
					<h3>My Packages</h3>
					<div class="suitcase-controls">
						<div class="float-left">
							<img src="images/suitcase-edit-blue.png">
							<img src="images/suitcase-user-blue.png">
							<img src="images/suitcase-comment-blue.png">
						</div>
						<div class="float-right">
							<img src="images/suitcase-email-blue.png">
						</div>
					</div>
				</div>
				<div class="deals-wrap">
					<div class="deal-block">
						<div class="deal-title">
							<img src="images/deal-img-01.jpg">
							<span class="close-icon"><img src="images/deal-close-icon.png"></span>
							<p>5 Star Deal</p>
							<span class="star-icon">
								<img src="images/star-rating.png">
								<img src="images/star-rating.png">
								<img src="images/star-rating.png">
							</span>
						</div>
						<div class="deal-desc">
							<div class="deal-date">
								<img src="images/deal-date.png">
							</div>
							<div class="view-deal">
								<img src="images/view-deal.png">
							</div>
							<!-- <div class="deal-price-quote">£1498</div>
							<div class="view-deal">VIEW DEAL</div> -->
						</div>
					</div>

					<div class="deal-block">
						<div class="deal-title">
							<img src="images/deal-img-01.jpg">
							<span class="close-icon"><img src="images/deal-close-icon.png"></span>
							<p>5 Star Deal</p>
							<span class="star-icon">
								<img src="images/star-rating.png">
								<img src="images/star-rating.png">
								<img src="images/star-rating.png">
							</span>
						</div>
						<div class="deal-desc">
							<div class="deal-date">
								<img src="images/deal-date.png">
							</div>
							<div class="view-deal">
								<img src="images/view-deal.png">
							</div>
							<!-- <div class="deal-price-quote">£1498</div>
							<div class="view-deal">VIEW DEAL</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(window).load(function(){
			$('#carousel').elastislide({easing : 'ease-in-out',speed : 500});
		});
		$('a.hajj').on('click', function(){
			$('.form-search-umrah').hide();
			$('.form-search-hajj').show();
		});
		$('a.umrah').on('click', function(){
			$('.form-search-hajj').hide();
			$('.form-search-umrah').show();
		});
		$('.form-search-umrah #search-btn-umrah').on('click', function(e){
			$('#search-result-umrah').slideDown('slow');
			if($('#search-btn-umrah').hasClass("click")) {
				$('form.form-search').submit();
			}
			$('#search-btn-umrah').addClass('click');
			$('#search-btn-umrah.click').val('Lets \n Compare');
		});
		$('.form-search-hajj #search-btn-hajj').on('click', function(e){
			$('#search-result-hajj').slideDown('slow');
			if($('#search-btn-hajj').hasClass("click")) {
				$('form.form-search').submit();
			}
			$('#search-btn-hajj').addClass('click');
			$('#search-btn-hajj.click').val('Lets \n Compare');
		});

		$('.button-trans').on('click', function(){
			$('.button-trans').removeClass('black');
			$(this).addClass('black');
		});

		var h1 = $('.right-sec-preview').height()>$('.right-sec-details').height()?$('.right-sec-preview').height():$('.right-sec-details').height();
		h1 = h1+50;
		$('.right-sec-details').height(h1);
		$('.right-sec-preview').height(h1);
		//var h2 = $('.right-sec-preview').height();
		// alert(h1);
		// alert(h2);
		$('.right-sec-details').height(h1);
		$('.daily-search').on('click', function() {
			$('.daily-search').removeClass('active');
			$(this).addClass('active');
		});
		if($(window).width() < 1000) {
			$('.left-section').addClass('slideClose');
			$('.left-section').removeClass('slideOpen');
		}
		$('.right-section-top').on('click', function() {
			if($('.side-search').hasClass('visible')){

				$('.side-search').removeClass('visible');
			}else {
				$('.search-page').toggleClass('open');
				$('.search-page').toggleClass('close');
				$('.search-page.close').animate({padding : '4px 15px 0 15px'});
				$('.search-page.open').animate({padding : '4px 15px 0 400px'});
			}
			$('.left-section').toggleClass('slideClose');
			$('.left-section').toggleClass('slideOpen');
			$('.left-section.slideClose').animate({left: '-100%'});
			$('.left-section.slideOpen').animate({left: '0'});

		});

		// $('.right-section').on('click', function(){
		// 	$('.left-section.slideClose').animate({left: '-100%'});
		// 	$('.left-section').removeClass('slideOpen');
		// });

		if($(window).width() < 640) {
			$('.right-section-top p').html('Filter Results');
		}

		$('.logo-title').on('click', function(){
			if($('.left-section').hasClass('slideClose')){
				$('.search-page').toggleClass('open');
				$('.search-page').toggleClass('close');
				$('.search-page.close').animate({padding : '4px 15px 0 15px'});
				$('.search-page.open').animate({padding : '4px 15px 0 400px'});
			} else {
				$('.left-section').addClass('slideClose');
				$('.left-section').removeClass('slideOpen');
				$('.left-section.slideClose').animate({left: '-100%'});
				$('.left-section.slideOpen').animate({left: '0'});
			}
			$('.side-search').toggleClass('visible');

		});
		/*$('.page-wrapper').on('click', function(e){
		 if(event.target.id!='side-search-btn'){
		 $('#side-search').removeClass('visible');
		 }
		 });*/
		$('#side-search-btn').on('click', function() {
			$('.desktop-hidden #side-search').toggleClass('visible');
		});

		$('.hajj.side-button-trans').on('click', function(){
			$('.side-search-hajj').slideToggle();
			$('.umrah.side-button-trans').toggle();
		});
		$('.umrah.side-button-trans').on('click', function(){
			$('.side-form-search-umrah').slideToggle();
			$('.hajj.side-button-trans').toggle();
		});
	</script>

	@endsection

