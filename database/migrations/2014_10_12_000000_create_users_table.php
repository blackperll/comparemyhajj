<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('company_name');
			$table->string('company_address');
			$table->string('company_tel_number');
			$table->string('your_name');
			$table->string('your_job_role');
			$table->string('contact_number');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('confirm_password', 60);
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
