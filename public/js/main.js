		$(window).load(function(){
			$('#carousel').elastislide({easing : 'ease-in-out',speed : 500});
		});		
		$('a.hajj').on('click', function(){
			$('.form-search-umrah').hide();
			$('.form-search-hajj').show();
		});
		$('a.umrah').on('click', function(){
			$('.form-search-hajj').hide();
			$('.form-search-umrah').show();
		});
		$('.form-search-umrah #search-btn-umrah').on('click', function(e){ 
			$('.form-search-umrah .search-details').slideUp('slow');
			$('#search-result-umrah').slideDown('slow');
			if($('#search-btn-umrah').hasClass("click")) {
				$('form.form-search').submit();
			}
			$('#search-btn-umrah').addClass('click');
			$('#search-btn-umrah.click').val('Lets \n Compare');			
		});
		$('.form-search-hajj #search-btn-hajj').on('click', function(e){ 
			$('.form-search-hajj .search-details').slideUp('slow');
			$('#search-result-hajj').slideDown('slow'); 
			if($('#search-btn-hajj').hasClass("click")) {
				$('form.form-search').submit();
			}
			$('#search-btn-hajj').addClass('click');
			$('#search-btn-hajj.click').val('Lets \n Compare');			
		});
		$('.circle').on('hover', function(){
			$('.bg01').toggleClass('hover');
		});
		$('.circle1').on('hover', function(){
			$('.bg02').toggleClass('hover');
		});
		$('.circle2').on('hover', function(){
			$('.bg03').toggleClass('hover');
		});
		$('.circle3').on('hover', function(){
			$('.bg04').toggleClass('hover');
		});
		$('.circle').on('click', function(){ 
			$('.opacity').removeClass('hover1');
			$('.bg01').addClass('hover1');
			$('.deal-form').hide();
			$('.form1').show(); 
		});
		$('.circle1').on('click', function(){ 
			$('.opacity').removeClass('hover1');
			$('.bg02').addClass('hover1');
			$('.deal-form').hide();
			$('.form2').show(); 
		});
		$('.circle2').on('click', function(){ 
			$('.opacity').removeClass('hover1');
			$('.bg03').addClass('hover1');
			$('.deal-form').hide();
			$('.form3').show(); 
		});
		$('.circle3').on('click', function(){ 
			$('.deal-form').hide();
			$('.form4').show();
			$('.opacity').removeClass('hover1');
			$('.bg04').addClass('hover1');
		});
		$('.circle4').on('click', function(){ 
			$('.deal-form').hide();
			$('.form5').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg05').addClass('hover1');
		});
		$('.circle5').on('click', function(){ 
			$('.deal-form').hide();
			$('.form6').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg06').addClass('hover1');
		});
		$('.circle6').on('click', function(){ 
			$('.deal-form').hide();
			$('.form7').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg07').addClass('hover1');
		});
		$('.circle7').on('click', function(){ 
			$('.deal-form').hide();
			$('.form8').show(); 
			$('.opacity').removeClass('hover1');
			$('.bg08').addClass('hover1');
		});
		$('.button-trans').on('click', function(){
			$('.button-trans').removeClass('black');
			$(this).addClass('black');
		});

		var wh = $(window).height(); 
		$('.section-full-screen').height(wh);
		$('.scroll-nav li').on('click', function() {
			if($(this).attr('class')=='screen-01'){
				$('.section-01').animate({
		        	bottom : '0'
		        }, 800);
		        $('.screen-02, .screen-03').removeClass('active');
		        $('.screen-01').addClass('active');
		        current_screen = 1;
			} else if($(this).attr('class')=='screen-02'){
				$('.section-01').animate({
		        	bottom : '100%'
		        }, 800);
				$('.section-02').animate({
		        	bottom : '0'
		        }, 800);
		        $('.screen-01, .screen-03').removeClass('active');
		        $('.screen-02').addClass('active');
		        current_screen = 2;
			} else if($(this).attr('class')=='screen-03'){
				$('.section-01').animate({
		        	bottom : '100%'
		        }, 800);
				$('.section-02').animate({
		        	bottom : '100%'
		        }, 800);
		        $('.screen-01, .screen-02').removeClass('active');
			    $('.screen-03').addClass('active');
		        current_screen = 3;
			}
		});
		var current_screen = 1;
		$('#foo').bind('mousewheel', function(e){
		    if(e.originalEvent.wheelDelta /120 > 0) {
		        // alert('up');
		        if (current_screen == 2) {
			        $('.section-01').animate({
			        	bottom : '0'
			        }, 800);
			        $('.screen-02, .screen-03').removeClass('active');
			        $('.screen-01').addClass('active');
			        current_screen = 1;
		    	} else if (current_screen == 3) {
			        $('.section-02').animate({
			        	bottom : '0'
			        }, 800);
			        $('.screen-01, .screen-03').removeClass('active');
			        $('.screen-02').addClass('active');
			        current_screen = 2;
		    	}
		    } 
		    else{
		        // alert('down');
		        if (current_screen == 1) {
		        $('.section-01').animate({
		        	bottom : '100%'
		        }, 800);
		        $('.screen-01, .screen-03').removeClass('active');
			    $('.screen-02').addClass('active');
		        current_screen = 2;
		    	} else if (current_screen == 2) {
			        $('.section-02').animate({
			        	bottom : '100%'
			        }, 800);
			        $('.screen-01, .screen-02').removeClass('active');
				    $('.screen-03').addClass('active');
			        current_screen = 3;
		    	};
		    }
		});

		$('#mobile-search-btn').on('click', function() { 
			$('.desktop-hidden #mobile-search').toggleClass('visible'); 
		});

		$(window).scroll(function() {
		    if ($(this).scrollTop() > 0) {
		    	// alert('s');
		    }
		});