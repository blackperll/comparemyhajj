<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	public $timestamps = false;
	protected $table = 'packages';
	protected $primaryKey ="idpackage";

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['package_title', 'package_star_rating', 'package_transport', 'package_airline', 'package_duration_total', 'package_duration_in_makka', 'package_hotel_in_makka', 'package_hotel_in_makka_star_rating', 'package_hotel_in_makka_distance','package_duration_in_madina','package_hotel_in_madina','package_hotel_in_madina_star_rating','package_hotel_in_madina_distance','package_flight_mode','package_price_per_person_two_person_room','package_price_per_person_three_person_room','package_price_per_person_four_person_room','package_start_date','package_end_date','package_ziyarat_in_makka','package_ziyarat_in_madina','package_description'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['idpackage', 'agencyid','LAST_UPDATE_TIMESTAMP'];

	public function images(){
		return $this->hasMany('App\Images',"idpackage","idpackage");
	}

}
