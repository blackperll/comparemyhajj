<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model {


    public $timestamps = false;
    protected $table = 'images';
    protected $primaryKey ="idimage";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['image_name', 'image_url', 'image_type'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['idpackage', 'idimage','LAST_UPDATE_TIMESTAMP'];

    public function images(){
        return $this->belongsTo('App\Packages',"idpackage","idpackage");
    }


}
