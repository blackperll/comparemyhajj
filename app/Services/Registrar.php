<?php namespace App\Services;

use App\User;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'company_name' => 'required|max:255',
			'company_address' => 'required|max:255',
			'company_tel_number' => 'required|max:10',
			'your_name' => 'required|max:50',
			'your_job_role' => 'required|max:30',
			'contact_number' => 'required|max:11',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6',
			
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return User::create([
			'company_name' => $data['company_name'],
			'company_address' => $data['company_address'],
			'company_tel_number' => $data['company_tel_number'],
			'your_name' => $data['your_name'],
			'your_job_role' => $data['your_job_role'],
			'contact_number' => $data['contact_number'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
			'confirm_password' => bcrypt($data['confirm_password']),
		]);
	}

}
