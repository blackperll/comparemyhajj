<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

//Route::get('home', 'HomeController@index');
Route::get('home', 'HomeController@index');
Route::get('resultpage', 'WelcomeController@resultpage');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*------------packages------------------*/
Route::get('addpackage','PackagesController@index');
Route::post('savepackage','PackagesController@savepackage');


Route::group(array('prefix' => 'gallery'), function () {
	$resource   = 'gallery';
	$controller = 'GalleryimagesController@';

	// ...

	Route::post('{id}', array('as' => $resource.'.postUpload', 'uses' => $controller.'postUpload' ));

	// ...
});
Route::get('package/uploadgalleryimages',array('uses' => 'GalleryimagesController@index'));
Route::get('package/createdsuccessfully', array('uses' => 'PackagesController@created'));
Route::get('package/deletefile/fid={id}', array('uses' => 'GalleryimagesController@deletefile'));
Route::get('packages/manage',array('uses' => 'PackagesController@manage'));
