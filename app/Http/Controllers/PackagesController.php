<?php namespace App\Http\Controllers;
use App\Airline;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Packages;
use App\Transport;
use App\Images;
use Session;
use Auth;



class PackagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$transports = Transport::where('status','=','1')->get();
		$airlines = Airline::where('status','=','1')->get();
		return view('addpackage',array('transports' => $transports,'airlines' => $airlines));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function savepackage()
	{
		$data=Input::all();
  //print_r($data);die;
		$rules = array(
			'package_title' => 'required',
			'package_transport' => 'required',
			'package_airline' => 'required',
			'package_duration_total' => 'required'
		);
		$validator = Validator::make($data,$rules);
		if($validator->passes()){
            $packages = new Packages($data);
			$packages->agencyid = Auth::id();
			if($packages->save()){
				//save package id in session

				Session::put('pid',$packages->id);
				//check if image exists
				if(Input::hasFile('package_image')){
                    $destinationPath = 'packageImages/'.$packages->agencyid.'/'.$packages->id;
					$image = Input::file('package_image');
					$file = $image->getClientOriginalName();
					$filename = rand('11111','99999').'_'.$file;
					if($image->move($destinationPath,$filename)){
						$images = new Images;
						$images->idpackage = $packages->id;
						$images->image_name = $filename;
						$images->image_type = 'thumbnail';
						$images->image_url = $destinationPath;
                        if($images->save()){
							return Redirect('package/uploadgalleryimages');
						}
					}
				}else{
					echo "noimage";die;
				}
			}
	    }else{

		}

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function created()
	{


		return view('packagecreated');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function manage()
	{
		$packages = Packages::all();

		$data_all = array();
		foreach ($packages as $package){

			//$data['package'][] =
			//print_r($data['package'] );die;

			//$data['package']['thumbnail'][] =
			$data = array(
                 'package' => Packages::find($package->idpackage),
				 'thumbnail' =>  Packages::find($package->idpackage)->images()->where('image_type','=','THUMBNAIL')->get(),
			);
			array_push($data_all,$data);

		}
          //print_r($data_all);die;
		return view('managepackages',array('data' => $data_all));
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
