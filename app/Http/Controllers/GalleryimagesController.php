<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Input;
use Validator;
use Response;
use Session;
use App\Images;

class GalleryimagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function postUpload($id){

		$input = Input::all();
		$rules = array(
			'file' => 'image|max:3000',
		);

		$validation = Validator::make($input, $rules);

		if ($validation->fails())
		{
			return Response::make($validation->errors->first(), 400);
		}

		$file               = Input::file('file');
		$destinationPath    = 'packageImages/'.Auth::id().'/'.$id.'/galleryimages/';

		// Get real extension according to mime type
		$ext                = $file->guessClientExtension();

		// Client file name, including the extension of the client
		$fullname           = $file->getClientOriginalName();

		// Hash processed file name, including the real extension
		$hashname           = date('H.i.s').'-'.md5($fullname).'.'.$ext;
		$upload_success     = Input::file('file')->move($destinationPath, $hashname);
		$models             = new Images;
		$models->image_name   = $hashname;
		$models->image_type = 'GALLERY';
		$models->image_url  =  $destinationPath;
		$models->idpackage  =  $id;
		$models->save();



		if( $upload_success ) {
			return Response::json(base64_encode($hashname), 200);
		} else {
			return Response::json('error', 400);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$id = Session::get('pid');

		return view('galleryimages',array('packageId' => $id));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function deletefile($fid)
	{
		$id = Session::get('pid');

		$file = base64_decode($fid);
		$targetPath    = 'packageImages/'.Auth::id().'/'.$id.'/galleryimages/';
		unlink($targetPath.$file);

		//delete file from database
		$file = Images::where('image_name','=',$file)->get()->first();

        //print_r($file->idimage);die;
		//$file = Images::where('image_name','=',$file)->get()->first();
		$file->delete();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
